<?php

namespace App\Http\Controllers;

use App\Event;
use App\Komentar;
use Illuminate\Http\Request;
use Auth;

use DB;
use Redirect;
class KomentarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $komentar = Komentar::all();
        
        $iduser= Auth::user()->id;  
        // dd($idprof);
        $idprof= Profile::select('id')->where("user_id",$iduser)->value('id');
        $idorg = Organizer::where('profile_id',$idprof)->first();
        $lokasi= DB::table('lokasi')->get();
        // dd($idorg);
    
        $kategori = DB::table('kategori')->get();
        // $event = Event::where('profile_id')
        
        
        return view('event.create',compact("lokasi","idorg","kategori"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
    		'komentar' => 'required',
    		'event_id' => 'required',
            'user_id' =>  'required',
            
    	]);
        $event_id = $request->event_id;
        $iduser= Auth::user()->id;
        Komentar::create([
    		'komentar' => $request->komentar,
    		'event_id' => $request->event_id,
            'user_id' => $iduser,
            
    	]);
        
    	return redirect()->route('Event.show', ['Event' => $event_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $komentar = Komentar::findorfail($id);
        $komentar->delete();
        $idevent= $komentar->value('event_id');
        
        return redirect()->route('Event.show', ['Event' => $idevent]);
    }
}
