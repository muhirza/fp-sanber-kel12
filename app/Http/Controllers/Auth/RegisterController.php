<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Organizer;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Profile;


use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'no_hp' => ['required', 'integer'],
            'NIK' => ['required', 'integer',],
            'alamat' => ['required', 'string'],
            'nama' => ['required', 'string'],
            'jenis' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user= User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        $profil = Profile::create([
            'no_hp' =>  $data['no_hp'],
            'NIK' =>  $data['NIK'],
            'alamat' => $data['alamat'],
            'user_id' => $user->id
        ]);
        Organizer::create([
            'nama' =>  $data['nama'],
            'jenis' =>  $data['jenis'],
            'profile_id' => $profil->id
        ]);
        // dd($data);
        return $user;
        
    }
}
