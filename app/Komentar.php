<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    //
    public $timestamps = false;
    protected $table = "Komentar";
    protected $fillable = ["user_id", "event_id","komentar"];

    public function event()
    {
        return $this->belongsTo('App\Event');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
