<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    public $timestamps = false;
    protected $table = "profile";
    protected $fillable = ["alamat", "no_hp","NIK","user_id"];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function organizer()
    {
        return $this->hasOne('App\Organizer');
    }
}
