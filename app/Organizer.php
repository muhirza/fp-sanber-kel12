<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organizer extends Model
{
    //
    public $timestamps = false;
    protected $table = "organizer";
    protected $fillable = ["nama", "jenis","profile_id"];

    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }
    public function event()
    {
        return $this->hasMany('App\Event');
    }
}
