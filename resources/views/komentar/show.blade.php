@extends('listco.master')

@section('content')
<!--? Hero Start -->
<div class="slider-area2">
         <div class="slider-height3  hero-overly hero-bg4 d-flex align-items-center">
               <div class="container">
                  <div class="row">
                     <div class="col-xl-12">
                           <div class="hero-cap2 pt-20 text-center">
                              <h2>Komentar</h2>
                           </div>
                     </div>
                  </div>
               </div>
         </div>
      </div>
      <!-- Hero End -->
<!--? Blog Area Start -->
<
                  
                  <div class="comments-area">
                     <div class="form-group">
                        <a class="btn btn-primary" href="{{ route('Komentar.index') }}" role="button">Go Back</a>
                     </div> 
                     @forelse($user as $key=>$val)
                     
                     <p>{{$val->nama}}</p>
                        @foreach($val->komentar as $item)
                           <div class="comment-list">
                              <div class="single-comment justify-content-between d-flex">
                                 <div class="user justify-content-between d-flex">
                                    <div class="thumb">
                                       <img src="{{('listco/assets/img/blog/comment_1.png')}}" alt="">
                                    </div>
                                    <div class="desc">
                                       <!-- <h3 class="text-left my-3">Comments:</h3> -->
                                       
                                       <p class="comment"> 
                                          {{$item->komentar}}  <br>
                                                                                 
                                       </p>
                                       <div class="d-flex justify-content-between">
                                          <div class="d-flex align-items-center">
                                             <h5>
                                                <a href="#">Emily</a>
                                             </h5>                                             
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        @endforeach                           
                        @empty
                        <h2 class="text-center">No one comment</h2>
                     @endforelse
                  </div>
                  @auth
                  <div class="comment-form">
                     <h4>Leave a Reply</h4>
                     <form class="form-contact comment_form" method="post" action="{{ route('Komentar.store') }}" id="commentForm">
                        @csrf
                        <div class="row">
                           <div class="col-12">
                              <div class="form-group">
                                 <textarea class="form-control w-100" name="komentar" id="comment" cols="30" rows="9"
                                    placeholder="Write Comment"></textarea>
                              </div>
                           </div>
                           <div class="col-sm-3">
                              <div class="form-group">
                                 <input class="form-control" name="user_id" id="name" type="text" readonly='readonly' value="{{Auth::user()->name}}" placeholder="Name">
                              </div>
                              <div class="form-group">
                                 <input class="form-control" name="event_id" id="name" type="hidden" readonly='readonly' value="{{$event->id}}" placeholder="Name">
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        <div class="form-group">
                           <button type="submit" class="button button-contactForm btn_1 boxed-btn">Post Comment</button>
                        </div>
                     </form>
                  </div>
                  @endauth
                  @guest
                  <h2 class="text-center">Please login to comment</h2>
                  @endguest
               </div>
               
            </div>
         </div>
      </section>
@endsection