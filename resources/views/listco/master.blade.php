<!-- {{ asset('adminlte/plugins/fontawesome-free/css/all.min.css') }} -->
<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Event.io</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

	<!-- CSS here -->
	<link rel="stylesheet" href="{{asset ('listco/assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset ('listco/assets/css/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset ('listco/assets/css/slicknav.css')}}">
    <link rel="stylesheet" href="{{asset ('listco/assets/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset ('listco/assets/css/progressbar_barfiller.css')}}">
    <link rel="stylesheet" href="{{asset ('listco/assets/css/gijgo.css')}}">
    <link rel="stylesheet" href="{{asset ('listco/assets/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset ('listco/assets/css/animated-headline.css')}}">
	<link rel="stylesheet" href="{{asset ('listco/assets/css/magnific-popup.css')}}">
	<link rel="stylesheet" href="{{asset ('listco/assets/css/fontawesome-all.min.css')}}">
	<link rel="stylesheet" href="{{asset ('listco/assets/css/themify-icons.css')}}">
	<link rel="stylesheet" href="{{asset('listco/assets/css/slick.css')}}">
	<link rel="stylesheet" href="{{asset ('listco/assets/css/nice-select.css')}}">
	<link rel="stylesheet" href="{{asset('listco/assets/css/style.css')}}">
</head>
<body>
    <!-- ? Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="assets/img/logo/loder.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
    <header>
        <!-- Header Start -->
        <div class="header-area header-transparent">
            <div class="main-header header-sticky">
                <div class="container-fluid">
                    <div class="menu-wrapper d-flex align-items-center justify-content-between">
                        <!-- Logo -->
                        <!-- <div class="logo">
                            <a href="index.html"><img src="assets/img/logo/logo.png" alt=""></a>
                        </div> -->
                        <!-- Main-menu -->
                        <div class="main-menu f-right d-none d-lg-block">
                            <nav>
                                <ul id="navigation">
                                    <li><a href="/">Home</a></li> 
                                    <li><a href="#">Event</a>
                                        <ul class="submenu">
                                            @auth
                                            <li><a href="/Event/create">Create Event</a></li>
                                            @endauth
                                            <li><a href="/Event">Event List</a></li> 
                                        </ul>
                                    </li>
                                    <!-- <li><a href="blog.html">Blog</a>
                                        <ul class="submenu">
                                            <li><a href="blog.html">Blog</a></li>
                                            <li><a href="blog_details.html">Blog Details</a></li>
                                            <li><a href="elements.html">Elements</a></li>
                                        </ul>
                                    </li> -->
                                    <!-- <li><a href="contact.html">Contact</a></li> -->
                                </ul>
                            </nav>
                        </div>          
                        <!-- Header-btn -->
                        
                        <div class="header-btns d-none d-lg-block f-right">
                            {{-- <a href="#" class="mr-40"><i class="ti-user"></i> Log in</a>
                            <a href="#" class="btn">Register</a> --}}

                            @if (Route::has('login'))
                                <div class="top-right links">
                                    @auth
                                        {{-- <a href="{{ url('/') }}">{{Auth::user()->name}}</a> --}}
                                        {{-- <a href="#" class="mr-40"><i class="ti-user"></i>{{Auth::user()->name}}</a> --}}
                                        <div class="main-menu">
                                            <ul>
                                                <li><a href="#" class="mr-40"><i class="ti-user"></i>{{Auth::user()->name}}</a>
                                                    <ul class="submenu">
                                                        <li><a href="#">My Profile</a></li>
                                                        <li>
                                                            <a href="{{ route('logout') }}"
                                                                onclick="event.preventDefault();
                                                                            document.getElementById('logout-form').submit();">
                                                                {{ __('Logout') }}
                                                            </a>
                                                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                                                @csrf
                                                            </form>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        
                                    @else
                                        {{-- <a href="{{ route('login') }}">Login</a> --}}
                                        <a href="{{route('login')}}" class="mr-40"><i class="ti-user"></i> Log in</a>

                                        @if (Route::has('register'))
                                            {{-- <a href="{{ route('register') }}">Register</a> --}}
                                            <a href="{{route('register')}}" class="mr-40">Register</a>
                                        @endif
                                    @endauth
                                </div>
                            @endif
                        </div>
                        
                        <!-- Mobile Menu -->
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>
    <main>
        <!--? Hero Area Start-->
        @yield('content')
        
        
        <!--? Want To work 02-->
        <section class="wantToWork-area">
            <div class="container">
                <div class="wants-wrapper w-padding2">
                    <div class="row justify-content-between">
                        <div class="col-xl-8 col-lg-8 col-md-7">
                            <div class="wantToWork-caption wantToWork-caption2">
                                <img src="assets/img/logo/logo2_footer.png" alt="" class="mb-20">
                                <p>Users and submit their own items. You can create different packages and by connecting with your
                                    PayPal or Stripe account charge users for registration to your directory portal.</p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-5">
                            <div class="footer-social f-right sm-left">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                                <a href="https://bit.ly/sai4ull"><i class="fab fa-facebook-f"></i></a>
                                <a href="#"><i class="fab fa-pinterest-p"></i></a>
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Want To work End -->
        <!--? Want To work 01-->
        <section class="wantToWork-area">
            <div class="container">
                <div class="wants-wrapper">
                    <div class="row align-items-center justify-content-between">
                        <div class="col-xl-7 col-lg-9 col-md-8">
                            <div class="wantToWork-caption wantToWork-caption2">
                                <div class="main-menu2">
                                    <nav>
                                        <ul>
                                            <li><a href="index.html">Home</a></li>
                                            <li><a href="explore.html">Event</a></li> 
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
        <!-- Want To work End -->
    </main>
    <footer>
        <div class="footer-wrapper pt-30">
            <!-- footer-bottom -->
            <div class="footer-bottom-area">
                <div class="container">
                    <div class="footer-border">
                        <div class="row d-flex justify-content-between align-items-center">
                            <div class="col-xl-10 col-lg-9 ">
                                <div class="footer-copy-right">
                                    <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll Up -->
    <div id="back-top" >
        <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
    </div>

    <!-- JS here -->

    <script src="{{asset('listco/assets/js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="{{asset('listco/assets/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <script src="{{asset('listco/assets/js/popper.min.js')}}"></script>
    <script src="{{asset('listco/assets/js/bootstrap.min.js')}}"></script>
    <!-- Jquery Mobile Menu -->
    <script src="{{asset('listco/assets/js/jquery.slicknav.min.js')}}"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="{{asset('listco/assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('listco/assets/js/slick.min.js')}}"></script>
    <!-- One Page, Animated-HeadLin -->
    <script src="{{asset('listco/assets/js/wow.min.js')}}"></script>
    <script src="{{asset('listco/assets/js/animated.headline.js')}}"></script>
    <script src="{{asset('listco/assets/js/jquery.magnific-popup.js')}}"></script>

    <!-- Date Picker -->
    <script src="{{asset('listco/assets/js/gijgo.min.js')}}"></script>
    <!-- Nice-select, sticky -->
    <script src="{{asset('listco/assets/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('listco/assets/js/jquery.sticky.js')}}"></script>
    <!-- Progress -->
    <script src="{{asset('listco/assets/js/jquery.barfiller.js')}}"></script>
    
    <!-- counter , waypoint,Hover Direction -->
    <script src="{{asset('listco/assets/js/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('listco/assets/js/waypoints.min.js')}}"></script>
    <script src="{{asset('listco/assets/js/jquery.countdown.min.js')}}"></script>
    <script src="{{asset('listco/assets/js/hover-direction-snake.min.js')}}"></script>

    <!-- contact js -->
    <script src="{{asset('listco/assets/js/contact.js')}}"></script>
    <script src="{{asset('listco/assets/js/jquery.form.js')}}"></script>
    <script src="{{asset('listco/assets/js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('listco/assets/js/mail-script.js')}}"></script>
    <script src="{{asset('listco/assets/js/jquery.ajaxchimp.min.js')}}"></script>
    
    <!-- Jquery Plugins, main Jquery -->	
    <script src="{{asset('listco/assets/js/plugins.js')}}"></script>
    <script src="{{asset('listco/assets/js/main.js')}}"></script>


    
    </body>
</html>